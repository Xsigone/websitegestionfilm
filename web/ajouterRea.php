<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ajouter réalisateur</title>
  <link rel="stylesheet" href="sitecss.css" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
    <body>
        <?php require "header.php"; ?>
    <div class="container">
        <form action="action_ajouterRea.php">

            <label for="nomRea">Nom du réalisateur</label>
            <input type="text" id="nomr" name="nomrea" placeholder="Nom ..">
            <?php
            if (strpos($_GET['Error'], 'N')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ nom du réalisateur est vide.
            </div>
            <?php } ?>

            <label for="prenomRea">Prénom du réalisateur</label>
            <input type="text" id="prenomr" name="prenomrea" placeholder="Prenom ...">
            <?php
            if (strpos($_GET['Error'], 'P')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ prenom du réalisateur est vide.
            </div>
            <?php } ?>

            <input type="submit" value="Valider">

        </form>
    </div>
    <?php $file_db = null; ?>
    </body>
</html>
