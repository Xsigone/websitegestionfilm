<!doctype html>
<html>
    <head>
        <title>Explorer Film</title>
        <link rel="stylesheet" href="sitecss.css" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php require "header.php";?>
            <div class="flip-card-container">
            <?php
                $result = $file_db->query('SELECT * FROM FILM natural join REALISATEUR natural join GENRE order by anneesRea desc');
                $s = strtolower($_GET['search']);
                foreach ($result as $m){
                    if ($s=='all' || strpos(strtolower($m['nomFilm']), $s)!==False || strpos(strtolower($m['nomRealisateur']), $s)!==False || strpos(strtolower($m['prenomRealisateur']), $s)!==False || strpos(strtolower($m['nomGenre']), $s)!==False || $m['anneesRea']==$s){ ?>
                    <a class="boite-link" href='<?php echo "pageParFilm.php?idFilm=$m[idFilm]&nomFilm=$m[nomFilm]" ?>'>
                        <div class='boite'>
                            <div class='flip-card'>
                                <div class='flip-card-inner'>
                                    <div class='flip-card-front'>
                                        <img src='<?php echo"$m[lienPhoto]" ?>' alt=" <?php echo"$m[nomFilm]" ?>" style="width:300px;height:400px;">
                                    </div>
                                    <div class="flip-card-back">
                                        <?php
                                        echo"<h1>$m[nomFilm]</h1><p>$m[anneesRea]</p><p>$m[nomRealisateur] $m[prenomRealisateur]</p><p>$m[nomGenre]</p>"
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                <?php }} ?>
            </div>
        </div>
        <?php $file_db = null; ?>
    </body>
</html>