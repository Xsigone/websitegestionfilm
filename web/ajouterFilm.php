<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ajouter Film</title>
  <link rel="stylesheet" href="sitecss.css" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
    <body>
        <?php require "header.php"; ?>
    <div class="container">
        <form action="action_ajouterFilm.php">

            <label for="titref">Titre du Film</label>
            <input type="text" id="titref" name="titrefilm" placeholder="Titre ..">
            <?php
            if (strpos($_GET['Error'], 'T')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ titre du film est vide.
            </div>
            <?php } ?>

            <label for="annee">Année de Réalisation</label>
            <input class="inputNumber" type="number" id="annee" name="anneerea" placeholder="Exemple : 2019">
            <?php
            if (strpos($_GET['Error'], 'A')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ annee du film est vide.
            </div>
            <?php } ?>

            <label for="lienp">Lien de l'affiche</label>
            <input type="text" id="lienp" name="lienphoto">
            <?php
            if (strpos($_GET['Error'], 'L')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ lien de l'affiche du film est vide.
            </div>
            <?php } ?>
            
            <label for="synopsis">Synopsis</label>
            <input type="text" id="synopsis" name="synopsis">
            <?php
            if (strpos($_GET['Error'], 'S')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ du synopsis est vide.
            </div>
            <?php } ?>

            <label for="lienBA">Lien de la bande annonce</label>
            <input type="text" id="lienBA" name="lienBA">
            <?php
            if (strpos($_GET['Error'], 'B')){?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                <strong>Attention!</strong> Le champ lien de la bande annonce du film est vide.
            </div>
            <?php } ?>

            <label for="genre">Genre</label>
            <select id="genre" name="genre">
                <?php $result = $file_db->query('SELECT * FROM GENRE');
                foreach ($result as $m){$i+=1;
                    echo"<option value='$m[idGenre]'>$m[nomGenre]</option>";
                } ?>
            </select>

            <label for="realisateur">Réalisateur</label>
            <select id="realisateur" name="realisateur">
                <?php $result = $file_db->query('SELECT * FROM REALISATEUR');
                foreach ($result as $m){$i+=1;
                    echo"<option value='$m[idRealisateur]'>$m[nomRealisateur] $m[prenomRealisateur]</option>";
                } ?>
            </select>
            <a href='ajouterRea.php?Error=O'>Ajouter un réalisateur</a><BR>

            <input type="submit" value="Valider">

        </form>
    </div>
    <?php $file_db = null; ?>
    </body>
</html>
