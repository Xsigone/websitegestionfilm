<html>
    <head><title>Validation ajouter film</title></head>
    <body>
        <?php
            $file_db = new PDO("sqlite:../bd/film.sqlite");
            $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Tests
            $Error = ' ';
            if ($_GET['titrefilm'] == ''){
                $Error = $Error.'T';
            }
            if($_GET['anneerea'] == ''){
                $Error = $Error.'A';
            }
            if($_GET['lienphoto'] == ''){
                $Error = $Error.'L';
            }
            if($_GET['synopsis'] == ''){
                $Error = $Error.'S';
            }
            if($_GET['lienBA'] == ''){
                $Error = $Error.'B';
            }
            if ($Error!=' '){
                header('Location:ajouterFilm.php?Error='.$Error);
            }else{

            $insert = "INSERT INTO FILM (idFilm, nomFilm, anneesRea, idRealisateur, idGenre, lienPhoto, synopsis, lienBA) VALUES (:idFilm, :nomFilm, :anneesRea, :idRealisateur, :idGenre, :lienPhoto, :synopsis, :lienBA)";
            $stmt = $file_db->prepare($insert);

            $stmt->bindParam(':idFilm', $idFilm);
            $stmt->bindParam(':nomFilm', $nomFilm);
            $stmt->bindParam(':anneesRea', $anneesRea);
            $stmt->bindParam(':idRealisateur', $idRealisateur);
            $stmt->bindParam(':idGenre', $idGenre);
            $stmt->bindParam(':lienPhoto', $lienPhoto);
            $stmt->bindParam(':synopsis', $synopsis);
            $stmt->bindParam(':lienBA', $lienBA);
            $result = $file_db->query('SELECT idFilm FROM FILM');
            $idMax = 0;
            foreach ($result as $m){
                if ($m['idFilm']>$idMax){
                    $idMax = $m['idFilm'];
                }
            }
            $idFilm = $idMax+1;
            $nomFilm = $_GET['titrefilm'];
            $anneesRea = $_GET['anneerea'];
            $idRealisateur = $_GET['realisateur'];
            $idGenre = $_GET['genre'];
            $lienPhoto = $_GET['lienphoto'];
            $synopsis = $_GET['synopsis'];
            $lienBA = $_GET['lienBA'];
            $stmt->execute();
            header('Location:site.php');
        }
        ?>
    </body>
</html>