<html>
    <head><title>Validation ajouter film</title></head>
    <body>
        <?php
            $file_db = new PDO("sqlite:../bd/film.sqlite");
            $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Tests
            $Error = ' ';
            if ($_GET['nomrea'] == ''){
                $Error = $Error.'N';
            }
            if($_GET['prenomrea'] == ''){
                $Error = $Error.'P';
            }
            if ($Error!=' '){
                header('Location:ajouterRea.php?Error='.$Error);
            }else{

            $insert = "INSERT INTO REALISATEUR (idRealisateur, nomRealisateur, prenomRealisateur) VALUES (:idRealisateur, :nomRealisateur, :prenomRealisateur)";
            $stmt = $file_db->prepare($insert);

            $stmt->bindParam(':idRealisateur', $idRealisateur);
            $stmt->bindParam(':nomRealisateur', $nomRealisateur);
            $stmt->bindParam(':prenomRealisateur', $prenomRealisateur);
            $result = $file_db->query('SELECT idRealisateur FROM REALISATEUR');
            $idMax = 0;
            foreach ($result as $m){
                if ($m['idRealisateur']>$idMax){
                    $idMax = $m['idRealisateur'];
                }
            }
            $idRealisateur = $idMax+1;
            $nomRealisateur = $_GET['nomrea'];
            $prenomRealisateur = $_GET['prenomrea'];
            $stmt->execute();
            header('Location:ajouterFilm.php?Error=O');
        }
        ?>
    </body>
</html>