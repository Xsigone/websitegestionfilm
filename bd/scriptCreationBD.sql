drop table FILM;
drop table REALISATEUR;
drop table GENRE;


create table REALISATEUR(
    idRealisateur number(3),
    nomRealisateur VarChar2(20),
    prenomRealisateur VarChar2(20),
    constraint KeyRea primary key (idRealisateur)
);

create table GENRE(
    idGenre number(2),
    nomGenre VarChar2(20),
    constraint KeyGenre primary key (idGenre)
);

create table FILM(
    idFilm number(5),
    nomFilm VarChar2(200),
    anneesRea number(4),
    idRealisateur number(3),
    idGenre number(2),
    constraint KeyFilm primary key (idFilm),
    FOREIGN KEY (idRealisateur) REFERENCES REALISATEUR(idRealisateur),
    FOREIGN KEY (idGenre) REFERENCES GENRE(idGenre)
);
