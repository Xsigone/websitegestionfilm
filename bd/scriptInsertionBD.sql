insert into GENRE values(1, 'Anime');
insert into GENRE values(2, 'Policier');
insert into GENRE values(3, 'Horreur');
insert into GENRE values(4, 'Comedie');
insert into GENRE values(5, 'Action');
insert into GENRE values(6, 'Aventure');
insert into GENRE values(7, 'Science-Fiction');

insert into REALISATEUR values(1, 'Lefebvre', 'Hector');
insert into REALISATEUR values(2, 'Jestion', 'Louis');
insert into REALISATEUR values(3, 'Ron', 'Thomas');
insert into REALISATEUR values(4, 'Invisible', 'Chris');
insert into REALISATEUR values(5, 'LeBoss', 'Anthony');
insert into REALISATEUR values(6, 'LeBanc', 'Alban');
insert into REALISATEUR values(7, 'Mugiwala', 'Luffy');
insert into REALISATEUR values(8, 'Roronoa', 'Zoro');

insert into FILM values(1, 'Stampede', 2019, 8, 1);
insert into FILM values(2, 'YTP Franklin', 2017, 2, 4);
insert into FILM values(3, 'StareWareze', 2015, 1, 7);
insert into FILM values(4, 'MinecraftTheMovie', 2022, 3, 6);
insert into FILM values(5, 'Matrix 4', 2049, 1, 7);
insert into FILM values(6, 'La vie en noir', 1986, 4, 3);
insert into FILM values(7, 'Le Seigneur Des Pirates', 2000, 7, 1);
insert into FILM values(8, 'Tout ce que je n aime pas', 2019, 6, 4);
insert into FILM values(9, 'Eclatax le Film', 2019, 1, 2);
insert into FILM values(10, 'Fait pas le fou', 2019, 5, 4);

